function [] = reasonForCancelation()
%AIRPORTSWHITMORETRANSBORD Summary of this function goes here
%   Detailed explanation goes here
    clear();
    
    ds = datastore({'csv/1998.csv','csv/1999.csv','csv/2000.csv','csv/2001.csv','csv/2002.csv','csv/2003.csv','csv/2004.csv','csv/2005.csv','csv/2006.csv','csv/2007.csv','csv/2008.csv'},'TreatAsMissing', 'NA');
    
    ds.SelectedVariableNames = {'CancellationCode', 'Cancelled'};
    ds.SelectedFormats{strcmp(ds.SelectedVariableNames, 'CancellationCode')} = '%s'; 

    outds = mapreduce(ds, @countCancellationReasonMapper, @countCancellationReasonReducer);
    res = readall(outds);
    res = sortrows(res,2,'descend');

    cats = categorical(res{1:4,'Key'}); 
    cats = reordercats(cats, res{1:4,'Key'}); 
    res = varfun(@cell2mat, res(1:4,2)); 
    bar(cats, table2array(res)); 
    title('Top 4 Cancellation Reasons (1998 and 2008)'); 
    xlabel('Cancellation Reason') 
    ylabel('Number of Cancellation by Reasons')
    saveas(gcf,'CancellationsBarchartMapReduce.png'); 
    clear();
end


function countCancellationReasonMapper(data, ~, intermKVStore)
    %Se estandarizan los NA por NaN
    data.CancellationCode = standardizeMissing(data.CancellationCode,'NA');
    % Se eliminan los NaN
    cleared = rmmissing(data);
    code = [cleared.CancellationCode];
    cancelled = [cleared.Cancelled];
    [intermKeys,~, idxs] = unique(code, 'stable');
    intermVals = accumarray(idxs,cancelled,size(intermKeys),@countReason);
    addmulti(intermKVStore, intermKeys, num2cell(intermVals));

end

function out = countReason(cancelled)
    n = sum(cancelled == 1);
    out = n;
end

function countCancellationReasonReducer(intermKey, intermValIter, outKVStore)
    sum_occurences = 0;
    while(hasnext(intermValIter))
        sum_occurences = sum_occurences + getnext(intermValIter);
    end
    add(outKVStore, intermKey, sum_occurences); 
end


