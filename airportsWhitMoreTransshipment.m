function [] = airportsWhitMoreTransshipment()
%AIRPORTSWHITMORETRANSBORD Summary of this function goes here
%   Detailed explanation goes here
    ds = datastore({'csv/1998.csv','csv/1999.csv','csv/2000.csv','csv/2001.csv','csv/2002.csv','csv/2003.csv','csv/2004.csv','csv/2005.csv','csv/2006.csv','csv/2007.csv','csv/2008.csv'}, 'TreatAsMissing', 'NA'); 
    ds.SelectedVariableNames = {'Origin', 'Dest'}; 
    outds = mapreduce(ds, @countTransshipmentMapper, @countTransshipmentReducer);
    res = readall(outds);
    res = sortrows(res,2,'descend');

    cats = categorical(res{1:10,'Key'}); 
    cats = reordercats(cats, res{1:10,'Key'}); 
    res = varfun(@cell2mat, res(1:10,2)); 
    bar(cats, table2array(res)); 
    title('Top 10 airports with most traffic (1998 and 2008)'); 
    xlabel('Airport') 
    ylabel('Number of departures and arrivals')
    saveas(gcf,'AirportBarchartMapReduce.png'); 
 
end


function countTransshipmentMapper(data, ~, intermKVStore)
    all_codes = [data.Origin; data.Dest];       
    codes = unique(all_codes);       
    [~, idxs] = ismember(all_codes, codes);
    codes_count = arrayfun(@(x) sum(idxs == x), 1:numel(codes));
    addmulti(intermKVStore, codes, num2cell(codes_count)); 
end

function countTransshipmentReducer(intermKey, intermValIter, outKVStore)
    sum_occurences = 0;
    while(hasnext(intermValIter))
        sum_occurences = sum_occurences + getnext(intermValIter);
    end
    add(outKVStore, intermKey, sum_occurences); 
end


