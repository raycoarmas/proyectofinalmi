function [] = reasonForDelay()
%AIRPORTSWHITMORETRANSBORD Summary of this function goes here
%   Detailed explanation goes here
    clear();
    
    ds = datastore({'csv/1998.csv','csv/1999.csv','csv/2000.csv','csv/2001.csv','csv/2002.csv','csv/2003.csv','csv/2004.csv','csv/2005.csv','csv/2006.csv','csv/2007.csv','csv/2008.csv'},'TreatAsMissing', 'NA');
    ds.SelectedVariableNames = {'CarrierDelay', 'WeatherDelay', 'NASDelay','SecurityDelay','LateAircraftDelay'};

    outds = mapreduce(ds, @countDelayReasonMapper, @countDelayReasonReducer);
    res = readall(outds);
    res = sortrows(res,2,'descend');

    cats = categorical(res{1:5,'Key'}); 
    cats = reordercats(cats, res{1:5,'Key'}); 
    res = varfun(@cell2mat, res(1:5,2)); 
    bar(cats, table2array(res)); 
    title('Top 5 Delay Reasons (1998 and 2008)'); 
    xlabel('Delay Reason') 
    ylabel('Number of Delay by Reasons')
    saveas(gcf,'DelayBarchartMapReduce.png'); 
    clear();
end


function countDelayReasonMapper(data, ~, intermKVStore)
    % Se eliminan los NaN
    carrierDelay = [data.CarrierDelay];
    weatherDelay = [data.WeatherDelay];
    nASDelay = [data.NASDelay];
    securityDelay = [data.SecurityDelay];
    lateAircraftDelay = [data.LateAircraftDelay];
    
    carrierDelay = rmmissing(carrierDelay);
    weatherDelay = rmmissing(weatherDelay);
    nASDelay = rmmissing(nASDelay);
    securityDelay = rmmissing(securityDelay);
    lateAircraftDelay = rmmissing(lateAircraftDelay);
    
    carrierDelay = sum(carrierDelay);
    weatherDelay = sum(weatherDelay);
    nASDelay = sum(nASDelay);
    securityDelay = sum(securityDelay);
    lateAircraftDelay = sum(lateAircraftDelay);
    addmulti(intermKVStore, cellstr(['CarrierDelay     '; 'WeatherDelay     '; 'NASDelay         '; 'SecurityDelay    '; 'LateAircraftDelay']), num2cell([carrierDelay,weatherDelay,nASDelay,securityDelay,lateAircraftDelay]));

end
function countDelayReasonReducer(intermKey, intermValIter, outKVStore)
    sum_occurences = 0;
    while(hasnext(intermValIter))
        sum_occurences = sum_occurences + getnext(intermValIter);
    end
    add(outKVStore, intermKey, sum_occurences); 
end


